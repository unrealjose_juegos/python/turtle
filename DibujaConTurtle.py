import turtle
import os

#Prueba Git
#Prueba GitLab con GitHub Desktop

t=turtle.Pen()
Programa=True

while Programa:
	eleccion=int(input("Elige\n1->Avanzar\n2->Girar a la izquierda\n3->Girar a la derecha\n4->Color\n5->Circulos\n6->Mostrar/Quitar flecha\n7->Poner\Quitar Lapiz\n8->Deshacer la ultima accion\n9->Reiniciar\n10->Finalizar Programa\n"))
	if eleccion==1:
		Distancia=int(input("¿Que distancia recorrera?\n"))
		t.forward(Distancia)
	if eleccion==2:
		Giro=int(input("¿Cuantos grados quieres girar?\n"))
		t.left(Giro)
	if eleccion==3:
		Giro=int(input("¿Cuantos grados quieres girar?\n"))
		t.right(Giro)
	if eleccion==4:
		Color=input("¿Que color deseas?\n(Debes escribirlo en ingles(Primera letra en mayuscula)\n")
		t.pencolor(Color)
	if eleccion==5:
		Radio=int(input("Escribe el radio del circulo\n"))
		t.circle(Radio,None,None)
	if eleccion==6:
		Flecha=input("Escribe 'Q' para quitar la flecha\nEscribe 'M' para mostrar la flecha\n")
		Flecha=Flecha.lower()
		if Flecha=="q":
			t.hideturtle()
		if Flecha=="m":
			t.showturtle()
	if eleccion==7:
		Lapiz=input("Escribe 'P' para activar el lapiz o escriba 'Q' para quitarlo")
		Lapiz=Lapiz.lower()
		if Lapiz=='p':
			t.down()
		if Lapiz=='q':
			t.up()
	if eleccion==8:
		t.undo()
	if eleccion==9:
		Borrar=input("¿Estas seguro? (S/N)\n")
		Borrar=Borrar.lower()
		if Borrar=="s":
			t.reset()
	if eleccion==10:
		Cerrar=input("¿Estas seguro? (S/N)\n")
		Cerrar=Cerrar.lower()
		if Cerrar=="s":
			Programa=False

os.system('pause')